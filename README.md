Super Unity Build example
=========================

Example for Super Unity Build that creates Windows, Android and WebGL builds utilising Asset Bundles that are loaded asynchronously.
Also includes a DOS bat file to build from the command line.
Below are example output directory tree structure and the output from the program on the various platforms (showing the paths and defines).


File trees produced (example)
=============================

```
./2021-04-16/16/Debug/Android/Android(ARMv7,ETC2,Internal)/AirConsole/
./2021-04-16/16/Debug/Android/Android(ARMv7,ETC2,Internal)/AirConsole/BuildAutomationExp01.apk

./2021-04-16/16/Debug/PC/Windows x86/SteamEg/
./2021-04-16/16/Debug/PC/Windows x86/SteamEg/BuildAutomationExp01.exe
./2021-04-16/16/Debug/PC/Windows x86/SteamEg/BuildAutomationExp01_Data/ (usual Windows build stuff)
./2021-04-16/16/Debug/PC/Windows x86/SteamEg/BuildAutomationExp01_Data/StreamingAssets
./2021-04-16/16/Debug/PC/Windows x86/SteamEg/BuildAutomationExp01_Data/StreamingAssets/PC
./2021-04-16/16/Debug/PC/Windows x86/SteamEg/BuildAutomationExp01_Data/StreamingAssets/PC/Windows x86
./2021-04-16/16/Debug/PC/Windows x86/SteamEg/BuildAutomationExp01_Data/StreamingAssets/PC/Windows x86/myassetbundle.variant01
./2021-04-16/16/Debug/PC/Windows x86/SteamEg/BuildAutomationExp01_Data/StreamingAssets/PC/Windows x86/myassetbundle.variant01.manifest
./2021-04-16/16/Debug/PC/Windows x86/SteamEg/BuildAutomationExp01_Data/StreamingAssets/PC/Windows x86/Windows x86
./2021-04-16/16/Debug/PC/Windows x86/SteamEg/BuildAutomationExp01_Data/StreamingAssets/PC/Windows x86/Windows x86.manifest

./2021-04-16/16/Debug/WebGL/WebGL/AirConsole/BuildAutomationExp01/
./2021-04-16/16/Debug/WebGL/WebGL/AirConsole/BuildAutomationExp01/Build/ (usual WebGL build stuff)
./2021-04-16/16/Debug/WebGL/WebGL/AirConsole/BuildAutomationExp01/index.html
./2021-04-16/16/Debug/WebGL/WebGL/AirConsole/BuildAutomationExp01/StreamingAssets
./2021-04-16/16/Debug/WebGL/WebGL/AirConsole/BuildAutomationExp01/StreamingAssets/WebGL
./2021-04-16/16/Debug/WebGL/WebGL/AirConsole/BuildAutomationExp01/StreamingAssets/WebGL/WebGL
./2021-04-16/16/Debug/WebGL/WebGL/AirConsole/BuildAutomationExp01/StreamingAssets/WebGL/WebGL/myassetbundle.variant01
./2021-04-16/16/Debug/WebGL/WebGL/AirConsole/BuildAutomationExp01/StreamingAssets/WebGL/WebGL/myassetbundle.variant01.manifest
./2021-04-16/16/Debug/WebGL/WebGL/AirConsole/BuildAutomationExp01/StreamingAssets/WebGL/WebGL/WebGL
./2021-04-16/16/Debug/WebGL/WebGL/AirConsole/BuildAutomationExp01/StreamingAssets/WebGL/WebGL/WebGL.manifest
./2021-04-16/16/Debug/WebGL/WebGL/AirConsole/BuildAutomationExp01/TemplateData/ (usual stuff)
```

Output on various platforms
===========================

Windows
-------

```
Application.version     : 1.0.2296.3220
Application.platform    : WindowsPlayer
Application.absoluteURL : 
Application.GetBuildTags: []
BuildConstants.architecture: Windows_x86
BuildConstants.platform    : PC
BuildConstants.releaseType : Debug
BuildConstants.distribution: SteamEg
BuildConstants.version     : 1.0.2296.3220
BuildConstants.buildDate   : 4/15/2021 1:25:04 PM
Application.dataPath           : D:/tmp/UnityBuilds/BuildAutomationExp/2021-04-15/9/Debug/PC/Windows x86/SteamEg/BuildAutomationExp01_Data
Application.persistentDataPath : C:/Users/User/AppData/LocalLow/Unreasonably Good Software/BuildAutomationExp01
Application.streamingAssetsPath: D:/tmp/UnityBuilds/BuildAutomationExp/2021-04-15/9/Debug/PC/Windows x86/SteamEg/BuildAutomationExp01_Data/StreamingAssets
Application.temporaryCachePath : C:/Users/User/AppData/Local/Temp/Unreasonably Good Software/BuildAutomationExp01
bundlesBaseURL                 : file://D:/tmp/UnityBuilds/BuildAutomationExp/2021-04-15/9/Debug/PC/Windows x86/SteamEg/BuildAutomationExp01_Data/StreamingAssets/PC/Windows x86/
DEBUG_UGS defined
Platform define: STANDALONE_WIN
Is 64-bit: false
```

Android
-------

```
Application.version     : 1.0.2296.3613
Application.platform    : Android
Application.absoluteURL : 
Application.GetBuildTags: []
BuildConstants.architecture: Android
BuildConstants.platform    : Android
BuildConstants.releaseType : Debug
BuildConstants.distribution: AirConsole
BuildConstants.version     : 1.0.2296.3613
BuildConstants.buildDate   : 04/15/2021 15:03:20
Application.dataPath           : /data/app/com.ugs.buildexp01-1/base.apk
Application.persistentDataPath : /storage/emulated/0/Android/data/com.ugs.buildexp01/files
Application.streamingAssetsPath: jar:file:///data/app/com.ugs.buildexp01-1/base.apk!/assets
Application.temporaryCachePath : /storage/emulated/0/Android/data/com.ugs.buildexp01/cache
bundlesBaseURL                 : jar:file:///data/app/com.ugs.buildexp01-1/base.apk!/assets/Android/Android/
DEBUG_UGS defined
Platform define: ANDROID
Is 64-bit: false
```

WebGL
-----

```
14:06:04.606 Application.version     : 1.0.2296.3228
14:06:04.612 Application.platform    : WebGLPlayer
14:06:04.612 Application.absoluteURL : http://localhost:55954/
14:06:04.612 Application.GetBuildTags: []
14:06:04.613 BuildConstants.architecture: WebGL
14:06:04.613 BuildConstants.platform    : WebGL
14:06:04.613 BuildConstants.releaseType : Debug
14:06:04.614 BuildConstants.distribution: AirConsole
14:06:04.614 BuildConstants.version     : 1.0.2296.3228
14:06:04.615 BuildConstants.buildDate   : 04/15/2021 13:27:04
14:06:04.615 Application.dataPath           : http://localhost:55954
14:06:04.615 Application.persistentDataPath : /idbfs/3fe00c45210ad960f61c5fdd1a9f1a38
14:06:04.616 Application.streamingAssetsPath: http://localhost:55954/StreamingAssets
14:06:04.616 Application.temporaryCachePath : /tmp
14:06:04.618 bundlesBaseURL                 : http://localhost:55954/StreamingAssets/WebGL/WebGL/
14:06:04.618 DEBUG_UGS defined
14:06:04.618 Platform define: WEBGL
14:06:04.618 Is 64-bit: false
```
