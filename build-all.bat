@rem Using Unity 2017.2 here but change this locally if you're using a later version
set PATH=C:\Program Files\Unity\Hub\Editor\2017.2.0f3\Editor;%PATH%

Unity -nographics -quit -batchmode -executeMethod SuperSystems.UnityBuild.BuildCLI.PerformBuild
