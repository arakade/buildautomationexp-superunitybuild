// This file is auto-generated. Do not modify or move this file.

public static class BuildConstants
{
    public enum ReleaseType
    {
        None,
        Debug,
    }

    public enum Platform
    {
        None,
        PC,
        Android,
        WebGL,
    }

    public enum Architecture
    {
        None,
        Windows_x86,
        Android,
        WebGL,
    }

    public enum Distribution
    {
        None,
        SteamEg,
        AirConsole,
    }

    public static readonly System.DateTime buildDate = new System.DateTime(637541049227669300);
    public const string version = "1.0.2296.4205";
    public const ReleaseType releaseType = ReleaseType.Debug;
    public const Platform platform = Platform.WebGL;
    public const Architecture architecture = Architecture.WebGL;
    public const Distribution distribution = Distribution.AirConsole;
}

