using System.Collections;
using System.IO;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Scripting;

[Preserve]
public class LoadAssetExample : MonoBehaviour {
	[SerializeField] private string assetBundleName = "MyBundleName";

	[SerializeField] private string assetName = "MyAssetName";

	//[SerializeField] private string bundlesBaseURL = "file://" + "StreamingAssets/";
	// TODO: How to have things based on build target?

	[SerializeField] private uint bundleCRC = 0;

	[SerializeField] private bool doSynchronous = true;

	private string bundlesBaseURL {
		get {
			var streamingAssetsPath = Application.streamingAssetsPath;
			// var firstLetter = streamingAssetsPath[0];
			// var protocolExtra = ('A' <= firstLetter || ) ? "file://" : "";
			var protocolRegex = new Regex("^(https?|jar|file):", RegexOptions.CultureInvariant | RegexOptions.Singleline | RegexOptions.IgnoreCase);
			var protocolExtra = protocolRegex.IsMatch(streamingAssetsPath) ? "" : "file://";
			return string.Format("{0}{1}/{2}/{3}/",
				protocolExtra,
				streamingAssetsPath,
				BuildConstants.platform,
				BuildConstants.architecture.ToString().Replace('_', ' ')
				);
		}
	}

	public void Start() {
		logPlatformEtc();
		if (doSynchronous) {
			loadSynchronous();
		} else {
			StartCoroutine(loadAsynchronous());
		}
	}

	private void logPlatformEtc() {
		// Unity-integrated bits
		// See https://docs.unity3d.com/ScriptReference/RuntimePlatform.html
		Debug.LogFormat("Application.version     : {0}", Application.version);
		Debug.LogFormat("Application.platform    : {0}", Application.platform);
		Debug.LogFormat("Application.absoluteURL : {0}", Application.absoluteURL);
		Debug.LogFormat("Application.GetBuildTags: [{0}]", string.Join(",", Application.GetBuildTags()));

		// Super Unity Build stuff
		Debug.LogFormat("BuildConstants.architecture: {0}", BuildConstants.architecture);
		Debug.LogFormat("BuildConstants.platform    : {0}", BuildConstants.platform);
		Debug.LogFormat("BuildConstants.releaseType : {0}", BuildConstants.releaseType);
		Debug.LogFormat("BuildConstants.distribution: {0}", BuildConstants.distribution);
		Debug.LogFormat("BuildConstants.version     : {0}", BuildConstants.version);
		Debug.LogFormat("BuildConstants.buildDate   : {0}", BuildConstants.buildDate);

		// Paths
		Debug.LogFormat("Application.dataPath           : {0}", Application.dataPath);
		Debug.LogFormat("Application.persistentDataPath : {0}", Application.persistentDataPath);
		Debug.LogFormat("Application.streamingAssetsPath: {0}", Application.streamingAssetsPath);
		Debug.LogFormat("Application.temporaryCachePath : {0}", Application.temporaryCachePath);
		Debug.LogFormat("bundlesBaseURL                 : {0}", bundlesBaseURL); // generated above

		// Arbitrary define added by build tool by myself
#if DEBUG_UGS
		Debug.Log("DEBUG_UGS defined");
#endif

#if UNITY_EDITOR
		const string PlatformDefine = "UNITY_EDITOR";
#elif UNITY_ANDROID
		const string PlatformDefine = "ANDROID";
#elif UNITY_IOS
		const string PlatformDefine = "IOS";
#elif UNITY_STANDALONE_OSX
		const string PlatformDefine = "OSX";
#elif UNITY_STANDALONE_WIN
		const string PlatformDefine = "STANDALONE_WIN";
#elif UNITY_STANDALONE_LINUX
		const string PlatformDefine = "STANDALONE_LINUX";
#elif UNITY_WEBGL
		const string PlatformDefine = "WEBGL";
#endif
		Debug.LogFormat("Platform define: {0}", PlatformDefine);

#if UNITY_64
		const string Is64Bit = "true";
#else
		const string Is64Bit = "false";
#endif
		Debug.LogFormat("Is 64-bit: {0}", Is64Bit);
	}

	private void loadSynchronous() {
		var myLoadedAssetBundle = AssetBundle.LoadFromFile(Path.Combine(Application.streamingAssetsPath, assetBundleName));
		if (myLoadedAssetBundle == null) {
			Debug.LogFormat("Failed to load AssetBundle \"{0}\"!", assetBundleName);
			return;
		}

		var prefab = myLoadedAssetBundle.LoadAsset<GameObject>(assetName);
		Instantiate(prefab);
	}

	private IEnumerator loadAsynchronous() {
		string uri = bundlesBaseURL + assetBundleName;
		Debug.LogFormat("URI:{0}", uri);
		UnityWebRequest request = UnityWebRequest.GetAssetBundle(uri, bundleCRC);
		Debug.LogFormat("request:{0}", request);
		// yield return request.Send();
		var asyncOperation = request.SendWebRequest();
		Debug.LogFormat("asyncOperation:{0}", asyncOperation);
		var limit = 1000;
		while (!asyncOperation.isDone) {
			Debug.LogFormat("progress:{0} - {1}", asyncOperation.progress, asyncOperation);
			yield return asyncOperation;
			if (limit-- <= 0) {
				Debug.LogErrorFormat("Timeout loading \"{0}\"", uri);
				yield break;
			}
		}

		AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(asyncOperation.webRequest); // request);
		Debug.LogFormat("bundle:{0}", bundle);
		if (null == bundle) {
			Debug.LogErrorFormat("Failed to load bundle from \"{0}\"", uri);
			yield break;
		}
		GameObject prefab = bundle.LoadAsset<GameObject>(assetName);
		Debug.LogFormat("prefab:{0}", prefab);
		if (null == prefab) {
			Debug.LogErrorFormat("Failed to load asset \"{1}\" from bundle from \"{0}\"", uri, assetName);
			yield break;
		}
		// GameObject sprite = bundle.LoadAsset<GameObject>("Sprite");
		Debug.LogFormat("instantiating");
		Instantiate(prefab);
		// Instantiate(sprite);

		Debug.LogFormat("done");
	}

}
