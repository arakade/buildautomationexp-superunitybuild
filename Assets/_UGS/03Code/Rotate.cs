﻿using UnityEngine;
using UnityEngine.Scripting;

[Preserve]
public class Rotate : MonoBehaviour {

	[SerializeField]
	private float rotation = 10f;

	public void Start() {
		myTransform = transform;
	}

	public void Update () {
		var r = myTransform.rotation.eulerAngles;
		r.y += (rotation * Time.deltaTime);
		myTransform.rotation = Quaternion.Euler(r);
	}

	private Transform myTransform;
}
